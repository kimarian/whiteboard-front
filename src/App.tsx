import React, { useState, useEffect, useRef } from 'react';
import './App.css';
import io from "socket.io-client";

// localhost:8888にwebsocketで接続する
// socketはwebsocketサーバーとの通信をコントロールするためのもの
const socket: SocketIOClient.Socket = io('localhost:8888');

// 線を描画するための型
type Point = {x: number, y:number};
type Line = {from: Point, to: Point};

// 線の太さ、線の色を指定する変数
const lineWidth = 7;
const lineColor = "#555555";

// Canvasコンポーネントに渡すプロパティ
interface CanvasProp {
  lines : Line[]
}

// 指定された線をCanvasに描画するコンポーネント
const Canvas = (prop: CanvasProp) => {
  
  // 描画されている<canvas>要素ノードへの参照を取得する
  const canvasRef = useRef(null);

  // canvasとしての操作を可能にするcanvas文脈オブジェクトをcanvasRefから取得する関数
  const getContext = (): CanvasRenderingContext2D => {
    const canvas: any = canvasRef.current;
    return canvas.getContext('2d');
  };

  // 再描画される毎に呼ぶ（AppでsetStateが呼ばれる度に呼ばれる）
  useEffect(() => {
    // canvas文脈オブジェクトを取得する
    // このオブジェクトを用いて線を描画していく
    const ctx: CanvasRenderingContext2D = getContext();

    // 前の結果を削除する
    ctx.clearRect(0, 0, 1000, 500);
    
    // ペンでの描画を開始する
    ctx.beginPath();

    // ペンの設定
    ctx.lineCap = "round";
    ctx.lineWidth = lineWidth * 2;
    ctx.strokeStyle = lineColor;

    // propで渡された線データをすべて描画する
    prop.lines.forEach(newline => {
      ctx.moveTo(newline.from.x, newline.from.y); // ペン先の座標移動
      ctx.lineTo(newline.to.x, newline.to.y); // 現在のペン先座標から指定座標へ線を引く
    });

    // 線を実際描画する
    ctx.stroke();
    ctx.save();
  });

  return (
    <div>
        <canvas className="canvas" width="1000px" height="500px" ref={canvasRef} />
    </div>
  );
}

// ReactはUIの状態をsetStateという関数を使って変更しそれに伴って再描画を実行する
// setStateを使って設定すると再描画がかかりすぎて重くなりすぎるため、stateとして使用する必要のない変数をここに退避
var press = false;
var cachedNewLine : Line[] = [];
var allLine : Line[] = [];

// アプリケーション全体のコンポーネント。これがindex.tsxの中で参照され実際にindex.htmlの挙動に反映される
const App = () => {

  // 現在の線列。linesに現在描画されている全線が入っているが、都合上allLineを使う
  const [lines, setLines] = useState<Line[]>([])
  const [prev, setPrev] = useState<Point>({x: 0, y: 0});

  // [なぜallLineがあるにもかかわらずlinesがあるのか]
　// useEffectに渡している関数（記法についてはTypeScript、Lambda式とかで検索しよう）はアプリケーション起動時一回だけ評価される。
  // よってconstが指定されているlinesが空列[]固定になってしまう
  // なのでallLineという変数へ線列をコピーしていろいろ処理する
  // しかしsetStateをよびださなければ再描画を発生させられないので用意している

  // アプリーケーション起動時一回だけ評価される関数を登録する
  useEffect(() => {

    // マウス左ボタンアップイベントにハンドラを登録する。
    // 補足: document.onmouseupメンバに関数（ハンドラ）を代入するとマウスの左ボタンが離されたらその関数が呼ばれるようになる
    // マウスの左ボタンが離されたら押されたときから記録されている線列をサーバーに送信する
    document.onmouseup = (e) => { 
      console.log("emitNewline...");

      press = false; // マウス左ボタンはなされたフラグ

      // 記録していた戦列をサーバーに送る
      // socketioではメッセージ名、メッセージデータを引数に指定してサーバーにいろいろ投げる
      socket.emit('emitNewLine', cachedNewLine); 
      cachedNewLine = [] // 送信したので初期化

    }

    // マウス左ボタンが押されたときの処理登録
    document.onmousedown = (e) => {
      press = true;
    }

    // socketioではメッセージ名ごとにハンドラを登録する形で投げられたデータをさばく
    // onメソッドを使ってそれを行う

    // サーバーから'receiveNewLine'メッセージが送られた来たときの処理
    socket.on('receiveNewLine', (newLines : Line[]) => {
      console.log("receiveNewline...");
      
      allLine = allLine.concat(newLines); // allLineに線列を保存
      setLines(allLine); // setLinesを読んで再描画を起こす
    }); 

    // サーバーから'responseAllDelete'メッセージが送られた来たときの処理
    socket.on('responseAllDelete', (data : {}) => {
      console.log("all delete");

      // 描画済み線をすべて削除
      cachedNewLine = []; 
      allLine = [];
      setLines(allLine);
    });
  }, [setPrev]); // ここのsetPrevに関しては https://qiita.com/takujiro_0529/items/1ddd5284692d52ac6be9を参照

  // マウスが移動したときに発生するイベント
  document.onmousemove = (e) => {
    // 現在のマウス位置を取得
    const cur = {x: e.pageX, y: e.pageY};

    if (press) { // 押されていたら線を登録していく。このとき前のマウス位置を参照する
    
      const newline = {from: prev, to: cur};
      cachedNewLine = cachedNewLine.concat([newline]);
      allLine = allLine.concat([newline]);
      setLines(allLine); // 描画主のクライアントはリアルタイムに線画描かれていったほうがいいので再描画をさせる
    }
    setPrev(cur);
  }  

  return (
    <div className="App">
      <Canvas lines={lines} />{/* linesを上の方で定義したCanvasコンポーネントに渡して描画する */}
      <button onClick={() => {socket.emit('requestAllDelete', {})}}>全部消す</button> {/* 全部けすボタンに全削除メッセージをサーバーに送るハンドラを登録しておく */}
    </div>
  );
}

// import App from "./App.tsx" とかするとAppコンポーネントがでてくるようにするやつ
export default App;
