import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

// public/index.htmlの<div id="root"></div>の内部要素をReactで書く
ReactDOM.render(
  <App />, // アプリ本体のコンポーネント(HTMLのUI定義)
  document.getElementById('root')
);
